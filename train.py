#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This script can be used for the training of MuCNN Model
#  
# python train.py --help can be used to learn how to use this script.
#
# usage: train.py [-h] [-r TF_RECORD_FILES [TF_RECORD_FILES ...]]
#                 [-b BATCH_SIZE] [-l LEARNING_RATE] [-n NB_EPOCH]
#                 [-s SHUFFLE_BUFFER_SIZE] [-sp SAVE_CHECKPOINT_PER_ITERATION]
#                 [-o OUT_DIR]
#
#
# optional arguments:
#   -h, --help            show this help message and exit
#   -r TF_RECORD_FILES [TF_RECORD_FILES ...], --tf_record TF_RECORD_FILES [TF_RECORD_FILES ...]
#                         tf-record files for training
#   -b BATCH_SIZE, --batch_size BATCH_SIZE
#                         batch size
#   -l LEARNING_RATE, --learning_rate LEARNING_RATE
#                         learning rate
#   -n NB_EPOCH, --nb_epoch NB_EPOCH
#                         number of epochs
#   -s SHUFFLE_BUFFER_SIZE, --shuffle_buffer_size SHUFFLE_BUFFER_SIZE
#                         shuffle buffer size
#   -sp SAVE_CHECKPOINT_PER_ITERATION, --save_checkpoint_per_iteration SAVE_CHECKPOINT_PER_ITERATION
#                         save checkpoint per number of iterations
#   -o OUT_DIR, --out_dir OUT_DIR
#                         output directory

from __future__ import print_function

SEED = 42

import random as rn
rn.seed(SEED)

import numpy as np
np.random.seed(SEED)

import tensorflow as tf
tf.set_random_seed(SEED)

import os
import argparse
from datasetEarth import datasetEarth
from utils import get_metrics
from tqdm import tqdm
from model import MuCNN

def run_training(
        tf_record_files, 
        batch_size, 
        learning_rate, 
        nb_epoch, 
        shuffle_buffer_size, 
        save_checkpoint_per_iteration,
        out_dir
    ):
    with tf.Session() as sess:
        dataset = datasetEarth(
            tf_record_files, 
            batch_size, 
            nb_epoch, 
            shuffle_buffer_size
        )
        
        iterator = dataset.get_batch_iterator()
        img_10m_gen, img_20m_gen, img_60m_gen, label_gen = iterator.get_next()

        model = MuCNN()
        model.create_network()
        loss = model.define_loss()

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)

        variables_to_save = tf.global_variables()
        metric_means, metric_update_ops = get_metrics(model.label, model.predictions, model.probabilities)
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        model_saver = tf.train.Saver(max_to_keep=0, var_list=variables_to_save)

        nb_iteration = 0
        batch_loss = 0.
        pbar = tqdm(desc = 'batch loss: ' + str(batch_loss))
        while True:
            try:
                img_10m_batch, img_20m_batch, img_60m_batch, label_batch = sess.run(
                    [img_10m_gen, img_20m_gen, img_60m_gen, label_gen]
                )
            except tf.errors.OutOfRangeError:
                break
            _, _, batch_loss = sess.run(
                [train_op, metric_update_ops, loss], 
                feed_dict = {
                        model.img_10m:img_10m_batch, 
                        model.img_20m:img_20m_batch, 
                        model.img_60m:img_60m_batch,
                        model.label:label_batch, 
                        model.is_training:True
                    }
                )
            nb_iteration += 1
            if (nb_iteration % save_checkpoint_per_iteration == 0):
                model_saver.save(sess, os.path.join(out_dir, 'iteration'), nb_iteration)
            pbar.set_description_str('batch loss: ' + str(batch_loss))
            pbar.update(1)
        model_saver.save(sess, os.path.join(out_dir, 'iteration'), nb_iteration)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description = 'MAML-RSIC training arguments')
    parser.add_argument(
        '-r', '--tf_record', dest = 'tf_record_files', 
        help = 'tf-record files for training', nargs = '+')
    parser.add_argument(
        '-b', '--batch_size', dest = 'batch_size', 
        help = 'batch size', type=int, default=1000)
    parser.add_argument(
        '-l', '--learning_rate', dest = 'learning_rate', 
        help = 'learning rate', type=float, default=1e-3)
    parser.add_argument(
        '-n', '--nb_epoch', dest = 'nb_epoch', 
        help = 'number of epochs', type=int, default=100)
    parser.add_argument(
        '-s', '--shuffle_buffer_size', dest = 'shuffle_buffer_size', 
        help = 'shuffle buffer size', type=int, default=0)
    parser.add_argument(
        '-sp', '--save_checkpoint_per_iteration', 
        dest = 'save_checkpoint_per_iteration', 
        help = 'save checkpoint per number of iterations', 
        type=int, default=1000)
    parser.add_argument(
        '-o', '--out_dir', dest = 'out_dir', 
        help = 'output directory', default='.')
    parser_args = parser.parse_args()

    tf_record_files = []
    for tf_record_file in parser_args.tf_record_files:
        tf_record_files.append(os.path.realpath(tf_record_file))

    run_training(
        tf_record_files, 
        parser_args.batch_size, 
        parser_args.learning_rate, 
        parser_args.nb_epoch, 
        parser_args.shuffle_buffer_size, 
        parser_args.save_checkpoint_per_iteration, 
        os.path.realpath(parser_args.out_dir)
    )
