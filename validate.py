#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# usage: validate.py [-h] [-r TF_RECORD_FILES [TF_RECORD_FILES ...]]
#                    [-d MODEL_DIR] [-b BATCH_SIZE]
#
# MuCNN validation arguments
#
# optional arguments:
#   -h, --help            show this help message and exit
#   -r TF_RECORD_FILES [TF_RECORD_FILES ...], --tf_record TF_RECORD_FILES [TF_RECORD_FILES ...]
#                         tf-record files for training
#   -d MODEL_DIR, --model_dir MODEL_DIR
#                         directory for model checkpoint files
#   -b BATCH_SIZE, --batch_size BATCH_SIZE
#                         batch size

from __future__ import print_function

SEED = 42

import random as rn
rn.seed(SEED)

import numpy as np
np.random.seed(SEED)

import tensorflow as tf
tf.set_random_seed(SEED)

import os
import argparse
from datasetEarth import datasetEarth
from utils import get_metrics, validate
from itertools import chain
from tqdm import tqdm
from model import MuCNN

def run_validation(
        tf_record_files, 
        batch_size, 
        model_dir
    ):
    with tf.Session() as sess:
        dataset = datasetEarth(
            tf_record_files, 
            batch_size, 
            1, 
            0
        )
        iterator = dataset.get_initializable_batch_iterator()
        img_10m_gen, img_20m_gen, img_60m_gen, label_gen = iterator.get_next()

        model = MuCNN()
        model.create_network()
        model.define_loss()
        model_path = tf.placeholder(tf.string)

        variables_to_restore = tf.global_variables() 
        metric_means, metric_update_ops = get_metrics(model.label, model.predictions, model.probabilities)
        variables_to_initialize = tf.global_variables()
        metric_best_models, best_model_update_ops = validate(metric_means, metric_update_ops, model_path)
        metric_best_model_vars = list(chain.from_iterable(metric_best_models))
        sess.run(tf.variables_initializer([metric_best_model_vars[i] for i in range(len(metric_best_model_vars)) if i % 3 != 0]))

        model_saver = tf.train.Saver(max_to_keep=0, var_list=variables_to_restore)
        model_files = [f.split('.')[0] for f in os.listdir(model_dir) if os.path.isfile(
            os.path.join(model_dir, f)) and '.meta' in f]
        model_files.sort(key = lambda x: int(x.split('-')[-1]))

        global_step = '-1'
        pbar = tqdm(total=len(model_files), desc = 'model step: ' + global_step)
        for model_id, model_file in enumerate(model_files):
            sess.run(tf.variables_initializer(variables_to_initialize))
            sess.run(tf.local_variables_initializer())
            model_saver.restore(sess, os.path.join(model_dir, model_file))
            global_step = model_file.split('-')[-1]
            sess.run(iterator.initializer)
            while True:
                try:
                    img_10m_batch, img_20m_batch, img_60m_batch, label_batch = sess.run([img_10m_gen, img_20m_gen, img_60m_gen, label_gen])
                except tf.errors.OutOfRangeError:
                    break

                _ = sess.run(
                        metric_update_ops + best_model_update_ops, 
                        feed_dict = {
                            model.img_10m:img_10m_batch, 
                            model.img_20m:img_20m_batch, 
                            model.img_60m:img_60m_batch,
                            model.label:label_batch, 
                            model.is_training:False,
                            model_path: os.path.join(model_dir, model_file)
                        }
                    )
            pbar.set_description_str('model step: ' + global_step)
            pbar.update(1)
        metric_best_model_res = sess.run([metric_best_model_vars[i] for i in range(len(metric_best_model_vars)) if i % 3 != 0])

        for i in range(0, len(metric_best_model_vars), 3):
            print(metric_best_model_vars[i], [str(metric_best_model_res[2 * i/3]), metric_best_model_res[2 * i/3 + 1]])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description = 'MAML-RSIC validation arguments')
    parser.add_argument(
        '-r', '--tf_record', dest = 'tf_record_files', 
        help = 'tf-record files for training', nargs = '+')
    parser.add_argument(
        '-d', '--model_dir', dest = 'model_dir', 
        help = 'directory for model checkpoint files', default='.')
    parser.add_argument(
        '-b', '--batch_size', dest = 'batch_size', 
        help = 'batch size', type=int, default=1000)

    parser_args = parser.parse_args()

    tf_record_files = []
    for tf_record_file in parser_args.tf_record_files:
        tf_record_files.append(os.path.realpath(tf_record_file))

    run_validation(
        tf_record_files, 
        parser_args.batch_size, 
        os.path.realpath(parser_args.model_dir)
    )
