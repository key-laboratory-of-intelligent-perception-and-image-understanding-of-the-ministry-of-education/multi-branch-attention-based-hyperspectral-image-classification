# HyperSpectral Image Classification using Multi-Branch CNN with BiLSTM based Attention

This repository contains code for the research project at the Key Laboratory of Intelligent Perception and Image Understanding of Ministry of Education(China) on `HyperSpectral Image Classification using Multi-Branch CNN with BiLSTM based Attention`, this work is being utilised for our current research on Caption Generation for Remote Sensing Images.  

Research Group: Key Laboratory of Intelligent Perception and Image Understanding of Ministry of Education, China 
Author: [Divyanshu Daiya](http://divyanshudaiya.ml).

![](Model.png)

Prerequisites
* TensorFlow and tqdm packages should be installed. Python 2.7, TensorFlow 1.3. 

See the dataset download and processing instructions in 'splitClasses.py'.
Indian Pine Dataset can be loaded with modifications in 'splitClasses.py. 


## For Training

The script `train.py` expects the following command line arguments:
* `-r`, `--tf_record`: `TFRecord` file(s) for training.
* `-b`, `--batch_size`: Batch size used during training.
* `-l`, `--learning_rate`: The initial learning rate.
* `-n`, `--nb_epoch`: The number of epochs for the training.
* `-s`, `--shuffle_buffer_size`: The number of elements which will be shuffled at the beginning of each epoch. It is not recommended to have large shuffle buffer if you don't have enough space in memory. 
* `-sp`, `--save_checkpoint_per_iteration`: The number of iterations per which a checkpoint is written, i.e., when `iteration_index % save_checkpoint_per_iteration == 0`.
* `-o`, `--out_dir`: The directory path where all checkpoints will be saved.

## For Validation
The script `validate.py` expects the following command line arguments:
* `-r`, `--tf_record`: `TFRecord` file(s) for validation.
* `-d`, `--model_dir`: The directory path where the checkpoints were saved for validation.
* `-b`, `--batch_size`: Batch size used during validation.

## For Evaluation
The script `eval.py` expects the following command line arguments:
* `-r`, `--tf_record`: `TFRecord` file(s) for evaluation.
* `-m`, `--model_file`: The base name of a pre-trained model snapshot (i.e., checkpoint).
* `-b`, `--batch_size`: Batch size used during evaluation.

## Author
**Divyanshu Daiya**
https://divyanshudaiya.ml

## License
The code in this repository is licensed under the **MIT License**.


