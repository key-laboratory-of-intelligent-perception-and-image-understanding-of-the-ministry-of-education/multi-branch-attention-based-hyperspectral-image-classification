#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This class is for "MuCNN" model.
# 
#
# Author: Key Lab IPIU, China
# Divyanshu Daiya, http://divyanshudaiya.ml

SEED = 42

import random as rn
rn.seed(SEED)

import numpy as np
np.random.seed(SEED)

import tensorflow as tf
tf.set_random_seed(SEED)

class MuCNN:
    def __init__(self):
        self.feature_size = 128
        self.nb_bands_10m = 4
        self.nb_bands_20m = 6
        self.nb_bands_60m = 2
        self.prediction_threshold = 0.5
        self.nb_class = 43
        self.is_training = tf.placeholder(tf.bool, [])
        self.img_10m = tf.placeholder(tf.float32, shape= [None, 120, 120, 4])
        self.img_20m = tf.placeholder(tf.float32, shape= [None, 60, 60, 6])
        self.img_60m = tf.placeholder(tf.float32, shape= [None, 20, 20, 2])
        self.label = tf.placeholder(tf.float32, shape=(None, self.nb_class))

    def feed_dict(self, img_10m_batch, img_20m_batch, img_60m_batch, label_batch, is_training=False):
        return {
                self.img_10m:img_10m_batch, 
                self.img_20m:img_20m_batch, 
                self.img_60m:img_60m_batch,
                self.label:label_batch, 
                self.is_training:is_training,
            }

    def define_loss(self):
        self.loss = tf.losses.sigmoid_cross_entropy(multi_class_labels=self.label, logits=self.logits) 
        return self.loss

    def fully_connected_block(self, inputs, nb_neurons, is_training, name):
        with tf.variable_scope(name):
            fully_connected_res = tf.layers.dense(
                inputs = inputs,
                units = nb_neurons,
                activation = None,
                use_bias = True,
                kernel_initializer= tf.contrib.layers.xavier_initializer_conv2d(seed = SEED),
                bias_initializer=tf.zeros_initializer(),
                kernel_regularizer=tf.contrib.layers.l2_regularizer(2e-5),
                bias_regularizer=None,
                activity_regularizer=None,
                trainable=True,
                name = 'fc_layer',
                reuse=None
            )
            batch_res = tf.layers.batch_normalization(inputs = fully_connected_res, name = 'batch_norm', training = is_training)
            return tf.nn.relu(features = batch_res, name = 'relu')

    def conv_block(self, inputs, nb_filter, filter_size, is_training, name):
        with tf.variable_scope(name):
            conv_res = tf.layers.conv2d(
                inputs = inputs,
                filters = nb_filter,
                kernel_size = filter_size,
                strides = (1, 1),
                padding = 'same',
                data_format = 'channels_last',
                dilation_rate = (1, 1),
                activation = None,
                use_bias = True,
                kernel_initializer = tf.contrib.layers.xavier_initializer_conv2d(seed = SEED),
                bias_initializer = tf.zeros_initializer(),
                kernel_regularizer = tf.contrib.layers.l2_regularizer(2e-5),
                bias_regularizer = None,
                activity_regularizer = None,
                trainable = True,
                name = 'conv_layer',
                reuse = None
            )
            batch_res = tf.layers.batch_normalization(inputs = conv_res, name = 'batch_norm', training = is_training)
            return tf.nn.relu(features = batch_res, name = 'relu')

    def pooling(self, inputs, name):
        return tf.nn.max_pool(
            value = inputs,
            ksize = [1, 2, 2, 1],
            strides = [1, 2, 2, 1],
            padding = "VALID",
            data_format ='NHWC',
            name = name
        )

    def dropout(self, inputs, drop_rate, is_training, name):
        return tf.layers.dropout(
                inputs,
                rate = drop_rate,
                noise_shape = None,
                seed = SEED,
                training = is_training,
                name = name
            )

    def branch_model_10m(self, inputs, is_training):
        with tf.variable_scope('CNN_10m_branch'):
            out = self.conv_block(inputs, 32, [5,5], is_training, 'conv_block_0')
            out = self.pooling(out, 'max_pooling')
            out = self.dropout(out, 0.25, is_training, 'dropout_0')
            out = self.conv_block(out, 32, [5,5], is_training, 'conv_block_1')
            out = self.pooling(out, 'max_pooling_1')
            out = self.dropout(out, 0.25, is_training, 'dropout_1')
            out = self.conv_block(out, 64, [3,3], is_training, 'conv_block_2')           
            out = self.dropout(out, 0.25, is_training, 'dropout_2')
            out = tf.contrib.layers.flatten(out)
            out = self.fully_connected_block(out, self.feature_size, is_training, 'fc_block_0')
            feature = self.dropout(out, 0.5, is_training, 'dropout_3')
            return feature 

    def branch_model_20m(self, inputs, is_training):
        with tf.variable_scope('CNN_20m_branch'):
            out = self.conv_block(inputs, 32, [3,3], is_training, 'conv_block_0')
            out = self.pooling(out, 'max_pooling_0')
            out = self.dropout(out, 0.25, is_training, 'dropout_0')
            out = self.conv_block(out, 32, [3,3], is_training, 'conv_block_1')
            out = self.dropout(out, 0.25, is_training, 'dropout_1')
            out = self.conv_block(out, 64, [3,3], is_training, 'conv_block_2')
            out = self.dropout(out, 0.25, is_training, 'dropout_2')
            out = tf.contrib.layers.flatten(out)
            out = self.fully_connected_block(out, self.feature_size, is_training, 'fc_block_0')
            feature = self.dropout(out, 0.5, is_training, 'dropout_3')  
            return feature

    def branch_model_60m(self, inputs, is_training):
        with tf.variable_scope('CNN_60m_branch'):
            out = self.conv_block(inputs, 32, [2,2], is_training, 'conv_block_0')
            out = self.dropout(out, 0.25, is_training, 'dropout_0')
            out = self.conv_block(out, 32, [2,2], is_training, 'conv_block_1')
            out = self.dropout(out, 0.25, is_training, 'dropout_1')
            out = self.conv_block(out, 32, [2,2], is_training, 'conv_block_2')
            out = self.dropout(out, 0.25, is_training, 'dropout_2')
            out = tf.contrib.layers.flatten(out)
            out = self.fully_connected_block(out, self.feature_size, is_training, 'fc_block_0')
            feature = self.dropout(out, 0.5, is_training, 'dropout_3') 
            return feature

    def create_network(self):
        patch_length_10m = 30
        patch_length_20m = 15
        patch_length_60m = 5
        padding_10m = (patch_length_10m - (120 % patch_length_10m)) if 120 % patch_length_10m != 0 else 0
        padding_20m = (patch_length_20m - (60 % patch_length_20m)) if 60 % patch_length_20m != 0 else 0
        padding_60m = (patch_length_60m - (20 % patch_length_60m)) if 20 % patch_length_60m != 0 else 0
        self.nb_patch = ((120 + padding_10m) / patch_length_10m)**2
       
        branch_features = []
        for img_bands, patch_length, nb_bands, branch_model, padding in zip(
            [self.img_10m, self.img_20m, self.img_60m], 
            [patch_length_10m, patch_length_20m, patch_length_60m],
            [self.nb_bands_10m, self.nb_bands_20m, self.nb_bands_60m],
            [self.branch_model_10m, self.branch_model_20m, self.branch_model_60m],
            [padding_10m, padding_20m, padding_60m]
            ):
            patches = tf.space_to_batch_nd(img_bands,[patch_length , patch_length],[[0,padding],[0,padding]])
            patches = tf.split(patches, patch_length ** 2, 0)
            patches = tf.stack(patches,3)
            patches = tf.reshape(patches,[-1, patch_length, patch_length, nb_bands])
            branch_features.append(tf.reshape(branch_model(patches, self.is_training), [-1, self.nb_patch, self.feature_size]))

        with tf.variable_scope('feature_fusion'):
            patches_concat_embed_ = tf.concat(branch_features, 2)
            patches_concat_embed_ = self.fully_connected_block(patches_concat_embed_, self.feature_size, self.is_training, 'fc_block_0')
            patches_concat_embed_ = self.dropout(patches_concat_embed_, 0.25, self.is_training, 'dropout_0')
        
        initializer = tf.contrib.layers.xavier_initializer(seed = SEED)
        with tf.variable_scope('bidirectional_LSTM'):
            cell_fw = tf.nn.rnn_cell.LSTMCell(1, initializer=initializer, name='lstm_cell_0')
            cell_bw = tf.nn.rnn_cell.LSTMCell(1, initializer=initializer, name='lstm_cell_1')

            inputs = [tf.reshape(tf.squeeze(i), [-1, self.feature_size]) for i in tf.split(patches_concat_embed_, self.nb_patch, 1)]
            bidirect_LSTM_res, _, _ = tf.contrib.rnn.static_bidirectional_rnn(
                cell_fw=cell_fw,
                cell_bw=cell_bw,
                inputs=inputs,
                dtype=tf.float32
            )
            multi_attention = []
            for i in bidirect_LSTM_res:
                fw_att, bw_att = tf.split(i, [1, 1], 1)
                attention = tf.divide(tf.add(fw_att, bw_att), 2.0)
                multi_attention.append(attention)
            self.multi_attention = tf.reshape(tf.stack(multi_attention, 1), [-1, self.nb_patch, 1])

        with tf.variable_scope('classification'):
            global_desc = []
            for single_att in tf.split(self.multi_attention, 1, -1):
                weighted_patches_concat_embed_ = tf.multiply(patches_concat_embed_, tf.tile(tf.nn.sigmoid(single_att), [1, 1, self.feature_size]))
                global_desc.append(tf.contrib.layers.flatten(weighted_patches_concat_embed_))
            global_desc = tf.add_n(global_desc)

            res = self.dropout(global_desc, 0.5, self.is_training, 'dropout_0')
            self.logits = tf.layers.dense(
                inputs = res,
                units = self.nb_class,
                activation=None,
                use_bias=True,
                kernel_initializer=initializer,
                bias_initializer=tf.zeros_initializer(),
                kernel_regularizer=None,
                bias_regularizer=None,
                activity_regularizer=None,
                trainable=True,
                name='fc_layer',
                reuse=None
            )
            self.probabilities = tf.nn.sigmoid(self.logits)
            self.predictions = tf.cast(self.probabilities >= self.prediction_threshold, tf.float32)
