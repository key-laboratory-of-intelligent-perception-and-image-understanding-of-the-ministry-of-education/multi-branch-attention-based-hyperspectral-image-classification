#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This script can be used for the evaluation of "MuCNN" model
# 
# python eval.py --help can be used to learn how to use this script.
# Author: Key Lab IPIU, China
# Divyanshu Daiya, http://divyanshudaiya.ml
# 
# usage: eval.py [-h] [-r TF_RECORD_FILES [TF_RECORD_FILES ...]] [-m MODEL_FILE]
#                [-b BATCH_SIZE]
#
# Evaluation arguments
#
# optional arguments:
#   -h, --help            show this help message and exit
#   -r TF_RECORD_FILES [TF_RECORD_FILES ...], --tf_record TF_RECORD_FILES [TF_RECORD_FILES ...]
#                         tf-record files for training
#   -m MODEL_FILE, --model_file MODEL_FILE
#                         model checkpoint file
#   -b BATCH_SIZE, --batch_size BATCH_SIZE
#                         batch size

from __future__ import print_function

SEED = 42

import random as rn
rn.seed(SEED)

import numpy as np
np.random.seed(SEED)

import tensorflow as tf
tf.set_random_seed(SEED)

import os
import argparse
from datasetEarth import datasetEarth
from utils import get_metrics
from model import MuCNN

def run_eval(
        tf_record_files, 
        batch_size, 
        model_file
    ):
    with tf.Session() as sess:
        dataset = datasetEarth(tf_record_files, batch_size, 1, 0)
        iterator = dataset.get_initializable_batch_iterator()

        img_10m_gen, img_20m_gen, img_60m_gen, label_gen = iterator.get_next()

        model = MuCNN()
        model.create_network()

        variables_to_restore = tf.global_variables() 
        
        metric_means, metric_update_ops = get_metrics(model.label, model.predictions, model.probabilities)
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        model_saver = tf.train.Saver(max_to_keep=0, var_list=variables_to_restore)
        model_saver.restore(sess, model_file)
        global_step = model_file.split('-')[-1]
        
        sess.run(iterator.initializer)
        while True:
            try:
                img_10m_batch, img_20m_batch, img_60m_batch, label_batch = sess.run([img_10m_gen, img_20m_gen, img_60m_gen, label_gen])
            except tf.errors.OutOfRangeError:
                eval_res = {}
                for metric in metric_means:
                    means = sess.run(
                            list(metric[1:]), 
                            feed_dict = {
                                    model.img_10m:img_10m_batch, 
                                    model.img_20m:img_20m_batch, 
                                    model.img_60m:img_60m_batch,
                                    model.label:label_batch, 
                                    model.is_training:False
                                }
                        )
                    eval_res[metric[0]] = [str(i) for i in means]
                    print(metric[0], means)
                break

            _, = sess.run(
                [metric_update_ops],                         
                feed_dict = {
                        model.img_10m:img_10m_batch, 
                        model.img_20m:img_20m_batch, 
                        model.img_60m:img_60m_batch,
                        model.label:label_batch, 
                        model.is_training:False
                    }
                )
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description = 'MAML-RSIC evaluation arguments')
    parser.add_argument(
        '-r', '--tf_record', dest = 'tf_record_files', 
        help = 'tf-record files for training', nargs = '+')
    parser.add_argument(
        '-m', '--model_file', dest = 'model_file', 
        help = 'model checkpoint file')
    parser.add_argument(
        '-b', '--batch_size', dest = 'batch_size', 
        help = 'batch size', type=int, default=1000)

    parser_args = parser.parse_args()

    tf_record_files = []
    for tf_record_file in parser_args.tf_record_files:
        tf_record_files.append(os.path.realpath(tf_record_file))

    run_eval(
        tf_record_files, 
        parser_args.batch_size, 
        os.path.realpath(parser_args.model_file)
    )
