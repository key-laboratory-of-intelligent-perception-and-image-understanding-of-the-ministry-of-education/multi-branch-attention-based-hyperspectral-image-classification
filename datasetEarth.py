import tensorflow as tf

class datasetEarth:
    def __init__(self, tf_record_paths, batch_size, nb_epoch, shuffle_buffer_size):
        dataset = tf.data.TFRecordDataset(tf_record_paths)
        if shuffle_buffer_size > 0:
            dataset = dataset.shuffle(buffer_size=shuffle_buffer_size)
        dataset = dataset.repeat(nb_epoch)

        dataset = dataset.map(
            self.parse_function, num_parallel_calls=10)

        dataset = dataset.batch(batch_size, drop_remainder=False)
        self.dataset = dataset.prefetch(10)
        self.iterator = self.dataset.make_one_shot_iterator()
        self.inializable_iterator = self.dataset.make_initializable_iterator()

    def get_batch_iterator(self):
        return self.iterator

    def get_initializable_batch_iterator(self):
        return self.inializable_iterator
        
    def parse_function(self, example_proto):
        parsed_features = tf.parse_single_example(
                example_proto, 
                {
                    'B01': tf.FixedLenFeature([20*20], tf.int64),
                    'B02': tf.FixedLenFeature([120*120], tf.int64),
                    'B03': tf.FixedLenFeature([120*120], tf.int64),
                    'B04': tf.FixedLenFeature([120*120], tf.int64),
                    'B05': tf.FixedLenFeature([60*60], tf.int64),
                    'B06': tf.FixedLenFeature([60*60], tf.int64),
                    'B07': tf.FixedLenFeature([60*60], tf.int64),
                    'B08': tf.FixedLenFeature([120*120], tf.int64),
                    'B8A': tf.FixedLenFeature([60*60], tf.int64),
                    'B09': tf.FixedLenFeature([20*20], tf.int64),
                    'B11': tf.FixedLenFeature([60*60], tf.int64),
                    'B12': tf.FixedLenFeature([60*60], tf.int64),
                    'original_labels_multi_hot': tf.FixedLenFeature([43], tf.int64)
                }
            )

        B01 = tf.cast(tf.reshape(parsed_features['B01'], [20, 20]), tf.float32)
        B02 = tf.cast(tf.reshape(parsed_features['B02'], [120, 120]), tf.float32)
        B03 = tf.cast(tf.reshape(parsed_features['B03'], [120, 120]), tf.float32)
        B04 = tf.cast(tf.reshape(parsed_features['B04'], [120, 120]), tf.float32)
        B05 = tf.cast(tf.reshape(parsed_features['B05'], [60, 60]), tf.float32)
        B06 = tf.cast(tf.reshape(parsed_features['B06'], [60, 60]), tf.float32)
        B07 = tf.cast(tf.reshape(parsed_features['B07'], [60, 60]), tf.float32)
        B08 = tf.cast(tf.reshape(parsed_features['B08'], [120, 120]), tf.float32)
        B8A = tf.cast(tf.reshape(parsed_features['B8A'], [60, 60]), tf.float32)
        B09 = tf.cast(tf.reshape(parsed_features['B09'], [20, 20]), tf.float32)
        B11 = tf.cast(tf.reshape(parsed_features['B11'], [60, 60]), tf.float32)
        B12 = tf.cast(tf.reshape(parsed_features['B12'], [60, 60]), tf.float32)
        multi_hot_label = parsed_features['original_labels_multi_hot']

        img_10m = tf.stack([B04, B03, B02, B08], axis=2)
        img_20m = tf.stack([B05, B06, B07, B8A, B11, B12], axis=2)
        img_60m = tf.stack([B01, B09], axis=2)
        
        return img_10m, img_20m, img_60m, multi_hot_label