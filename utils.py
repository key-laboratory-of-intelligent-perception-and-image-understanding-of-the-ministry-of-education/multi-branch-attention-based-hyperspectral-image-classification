#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

SEED = 42

import numpy as np
np.random.seed(SEED)

import tensorflow as tf
tf.set_random_seed(SEED)
import os
import json
import pickle
import h5py
import os
import random as rn

def validate(metric_means, metric_update_ops, model_file):
    best_model_update_ops = []
    best_models = []
    with tf.control_dependencies(metric_update_ops):
        for metrics in metric_means:
            metric_name = metrics[0]; metric_vals = metrics[1:]
            for i, metric_val in enumerate(metric_vals):
                best_val = tf.Variable(0., dtype=metric_val.dtype)
                best_model_file = tf.Variable('', dtype=tf.string)
                best_model_update_ops.append(
                    tf.assign(
                        best_model_file, 
                        tf.where(tf.greater_equal(metric_val, best_val), x = model_file, y = best_model_file)
                    )
                )
                best_model_update_ops.append(
                    tf.assign(best_val, tf.maximum(best_val, metric_val))
                )
                name_suffix = '' if len(metric_vals) == 1 else '_' + str(i)
                with tf.control_dependencies([best_model_update_ops[-1], best_model_update_ops[-2]]):
                    best_models.append((metric_name + name_suffix, best_val, best_model_file))
    return best_models, best_model_update_ops


def get_metrics(labels, predictions, probabilities):
    labels = tf.cast(labels, tf.float64)
    predictions = tf.cast(predictions, tf.float64)
    probabilities = tf.cast(probabilities, tf.float64)
    nb_ins = tf.shape(labels)[0]
    nb_class = tf.shape(labels)[1]
    with tf.variable_scope('metrics'):
        metric_update_ops = []
        zero = tf.constant(0., dtype=tf.float64)
        true_positive = tf.cast(tf.logical_and(tf.not_equal(predictions, zero), tf.not_equal(labels, zero)), tf.float64)
        false_positive = tf.cast(tf.logical_and(tf.not_equal(predictions, zero), tf.equal(labels, zero)), tf.float64)
        true_negative = tf.cast(tf.logical_and(tf.equal(predictions, zero), tf.equal(labels, zero)), tf.float64)
        false_negative = tf.cast(tf.logical_and(tf.equal(predictions, zero), tf.not_equal(labels, zero)), tf.float64)
        label_union_prediction = tf.cast(tf.logical_or(tf.not_equal(predictions, zero), tf.not_equal(labels, zero)), tf.float64) 
        label_cardinality = tf.reduce_sum(labels, 1)

        tp_class_accums = []
        for i in tf.unstack(true_positive, axis=1):
            init_class_tp = tf.Variable(0., dtype=tf.float64)
            metric_update_ops.append(tf.assign_add(init_class_tp, tf.reduce_sum(i)))
            tp_class_accums.append(init_class_tp)

        fp_class_accums = []
        for i in tf.unstack(false_positive, axis=1):
            init_class_fp = tf.Variable(0., dtype=tf.float64)
            metric_update_ops.append(tf.assign_add(init_class_fp, tf.reduce_sum(i)))
            fp_class_accums.append(init_class_fp)

        tn_class_accums = []
        for i in tf.unstack(true_negative, axis=1):
            init_class_tn = tf.Variable(0., dtype=tf.float64)
            metric_update_ops.append(tf.assign_add(init_class_tn, tf.reduce_sum(i)))
            tn_class_accums.append(init_class_tn)

        fn_class_accums = []
        for i in tf.unstack(false_negative, axis=1):
            init_class_fn = tf.Variable(0., dtype=tf.float64)
            metric_update_ops.append(tf.assign_add(init_class_fn, tf.reduce_sum(i)))
            fn_class_accums.append(init_class_fn)

        label_union_pred_class_accums = []
        for i in tf.unstack(label_union_prediction, axis=1):
            init_class_label_union_pred = tf.Variable(0., dtype=tf.float64)
            metric_update_ops.append(tf.assign_add(init_class_label_union_pred, tf.reduce_sum(i)))
            label_union_pred_class_accums.append(init_class_label_union_pred)

        sample_precision = tf.where(tf.equal(tf.reduce_sum(predictions, 1), zero),
                    x = tf.zeros(dtype=tf.float64, shape=nb_ins),
                    y = tf.divide(tf.reduce_sum(true_positive, 1), tf.reduce_sum(predictions, 1)))
        
        with tf.control_dependencies(metric_update_ops):
            nb_label = tf.add_n(tp_class_accums) + tf.add_n(fp_class_accums)
            micro_precision = tf.where(
                tf.equal(nb_label, zero),
                x = zero,
                y = tf.divide(tf.add_n(tp_class_accums), nb_label)
            )

        sample_recall = tf.where(tf.equal(label_cardinality, zero),
                    x = tf.zeros(dtype=tf.float64, shape=nb_ins),
                    y = tf.divide(tf.reduce_sum(true_positive, 1), label_cardinality))

        with tf.control_dependencies(metric_update_ops):
            nb_label = tf.add_n(tp_class_accums) + tf.add_n(fn_class_accums)
            micro_recall = tf.where(
                    tf.equal(nb_label, zero),
                    x = zero,
                    y = tf.divide(tf.add_n(tp_class_accums), nb_label)
                )

        with tf.control_dependencies(metric_update_ops):
            macro_recall_class = []
            for tp_class, fn_class in zip(tp_class_accums, fn_class_accums):
                nb_label_class = tp_class + fn_class
                macro_recall_class.append(
                    tf.where(tf.equal(nb_label_class, zero), x = zero, y = tf.divide(tp_class, nb_label_class))
                )
            macro_recall_class_avg = tf.reduce_mean(tf.stack(macro_recall_class))

        fbeta_score = lambda beta, precision, recall: tf.where(
            tf.equal(((beta * beta) * precision) + recall, zero),
            x = tf.zeros(dtype=tf.float64, shape=tf.shape(precision)),
            y = tf.divide((1. + (beta * beta)) * tf.multiply(precision, recall), ((beta * beta) * precision) + recall)
        )

        fbeta_score_core = lambda beta, tp, fp, tn, fn: tf.where(
            tf.equal(((1. + (beta * beta)) * tp )+ ((beta * beta) * fn) + fp, zero),
            x = zero,
            y = tf.divide((1. + (beta * beta)) * tp, ((1. + (beta * beta)) * tp )+ ((beta * beta) * fn) + fp)
        )

        sample_f2_score = fbeta_score(2., sample_precision, sample_recall)

        with tf.control_dependencies(metric_update_ops):
            micro_f2_score = fbeta_score(2., micro_precision, micro_recall)

        with tf.control_dependencies(metric_update_ops):
            macro_f2_score_class = []
            for tp_class, fp_class, tn_class, fn_class in zip(tp_class_accums, fp_class_accums, tn_class_accums, fn_class_accums):
                macro_f2_score_class.append(fbeta_score_core(2., tp_class, fp_class, tn_class, fn_class))
            macro_f2_score_class_avg = tf.reduce_mean(tf.stack(macro_f2_score_class))

        hamming_loss = tf.divide(
            tf.reduce_sum(
                tf.cast(
                    tf.logical_xor(
                        tf.cast(predictions, tf.bool), 
                        tf.cast(labels, tf.bool)
                    ), 
                    tf.float64
                ),
                1
            ), 
            tf.cast(nb_class, tf.float64)
        )

        row_indices = tf.range(nb_ins)
        col_indices = tf.argmax(probabilities, axis = 1, output_type=tf.int32)  
        one_error = tf.cast(tf.equal(tf.gather_nd(labels, tf.stack([row_indices, col_indices], axis = 1)), zero), tf.float64)

        ranking = tf.reduce_sum(
            tf.cast(
                tf.less_equal(
                        tf.tile(tf.expand_dims(probabilities, -1), [1, 1, nb_class]),
                        tf.reshape(
                            tf.tile(
                                probabilities, 
                                [1, nb_class]
                            ), 
                        [nb_ins, nb_class, nb_class]
                        )
                    ),
                tf.float64
            ),
            2
        )
        coverage = tf.reduce_max(
            tf.where(
                tf.not_equal(labels, zero), 
                x = ranking, 
                y = tf.zeros(dtype=tf.float64, shape=[nb_ins, nb_class])
            ), 
            1
        )

        one_probs = tf.where(tf.not_equal(labels, zero), x = probabilities, y = tf.ones(dtype=tf.float64, shape=[nb_ins, nb_class]) * 2)
        zero_probs = tf.where(tf.equal(labels, zero), x = probabilities, y = tf.ones(dtype=tf.float64, shape=[nb_ins, nb_class]) * -1)
        ranking_loss = tf.where(
                            tf.equal(label_cardinality * tf.reduce_sum(tf.cast(tf.equal(labels, zero), tf.float64), 1), zero), 
                            x = tf.zeros(dtype=tf.float64, shape=nb_ins),
                            y = tf.divide(
                                    tf.reduce_sum(  
                                        tf.cast(
                                            tf.less_equal(
                                                    tf.tile(tf.expand_dims(one_probs, -1), [1, 1, nb_class]), 
                                                    tf.reshape(
                                                        tf.tile(zero_probs, [1, nb_class]), 
                                                        [nb_ins, nb_class, nb_class]
                                                    )
                                                ),
                                            tf.float64
                                        ), 
                                        [1,2]
                                    ), 
                                    label_cardinality * tf.reduce_sum(tf.cast(tf.equal(labels, zero), tf.float64), 1)
                                )
                            )

        one_ranking_lower = tf.where(
            tf.equal(labels, zero), 
            x = tf.ones(dtype=tf.float64, shape=[nb_ins, nb_class]) * 100, 
            y = ranking
        )

        one_ranking_greater = tf.where(
            tf.equal(labels, zero), 
            x = tf.zeros(dtype=tf.float64, shape=[nb_ins, nb_class]) * -1, 
            y = ranking
        )

        label_ranking_avg_precision = tf.where(
            tf.equal(tf.reduce_sum(labels,1), zero),
            x = tf.zeros(dtype=tf.float64, shape=nb_ins),
            y = tf.divide(
                tf.reduce_sum(
                    tf.divide(
                        tf.reduce_sum(
                            tf.cast(
                                tf.less_equal(
                                    tf.tile(tf.expand_dims(one_ranking_lower, -1), [1, 1, nb_class]),
                                    tf.reshape(
                                        tf.tile(one_ranking_greater, [1, nb_class]), 
                                        [nb_ins, nb_class, nb_class]
                                    )
                                ),
                                tf.float64
                            ),
                            1
                        ),
                        ranking
                    ),
                    1
                ),
                tf.reduce_sum(labels,1)
            )
        )

    metrics_list_to_average = [
            ('sample_recall', sample_recall),  
            ('sample_f2_score',sample_f2_score), 
            ('hamming_loss', hamming_loss),
            ('one_error', one_error),
            ('coverage', coverage),
            ('ranking_loss', ranking_loss),
            ('label_ranking_avg_precision', label_ranking_avg_precision)
        ]

    metrics_list_to_accum = [
            ('micro_precision', micro_precision),  
            ('micro_recall', micro_recall), 
            ('macro_recall_class_avg', macro_recall_class_avg), 
            ('micro_f2_score', micro_f2_score), 
            ('macro_recall_class', macro_recall_class), 
            ('macro_f2_score_class', macro_f2_score_class), 
            ('macro_f2_score_class_avg', macro_f2_score_class_avg),
    ]

    metric_means = []
    
    for (name, var) in metrics_list_to_average:
        mean, update_op = tf.metrics.mean(var)
        metric_update_ops.append(update_op)
        metric_means.append((name, mean))

    for (name, metric_vars) in metrics_list_to_accum:
        metric_means.append((name,))
        if not type(metric_vars) == type([]):
            metric_means[-1] += (metric_vars,)
        else:
            for j, var in enumerate(metric_vars):
                metric_means[-1] += (var,)

    return metric_means, metric_update_ops
